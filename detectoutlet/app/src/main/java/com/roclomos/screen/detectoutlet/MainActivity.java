package com.roclomos.screen.detectoutlet;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.database.Cursor;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    private static Region ALL_ESTIMOTE_BEACONS = new Region("regionId", null, null, null);
    private BeaconManager beaconManager;// = new BeaconManager();
    String uuids,majors,minor,distan;
    int rqc=0;
    int i = 0;
    Beacon beacon;
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db=openOrCreateDatabase("beaconoutlet", Context.MODE_PRIVATE, null);
        final List<String> outlet = new ArrayList<String>();
        final List<String> distance = new ArrayList<String>();



        beaconManager= new BeaconManager(getApplicationContext());
        beaconManager.setBackgroundScanPeriod(
                TimeUnit.SECONDS.toMillis(3000), 0);



        beaconManager.setRangingListener(new BeaconManager.RangingListener() {

            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> beacons) {
                outlet.clear();
                distance.clear();
                //      TextView tv = (TextView) findViewById(R.id.loctxt);
                //    tv.setVisibility(View.INVISIBLE);
                String be = "", diste = "", major = "";
                // List<Integer> dist = new ArrayList<Integer>();
                int count = 0;
                double dist = 0;
                Beacon closest = null;

             //    Toast.makeText(MainActivity.this, db.toString(), Toast.LENGTH_SHORT).show();

                for (Beacon  beacon : beacons) {
                    //rqc++;
                    //Toast.makeText(MainActivity.this, "Connecting...", Toast.LENGTH_SHORT).show();
                    uuids=" "+beacon.getProximityUUID();//" b9407f30-f5f8-466e-aff9-25556b57fe6d";//
                    majors=" "+beacon.getMajor()+"";//" 50398";//
                   String minors=" "+beacon.getMinor();//" 21392";//
                   distan= String.format("%.2g%n", Utils.computeAccuracy(beacon));
                 //   Toast.makeText(MainActivity.this, uuids+" "+majors+" "+minor, Toast.LENGTH_SHORT).show();
                    String[] columns ={"outlet"};
                    //db.rawQuery("select * from beacon",null);
                    Cursor cursor = db.query("beacon", columns, "uuid=? and major=? and minor=?", new String[]{uuids,majors,minors}, null, null, null);
                   // Cursor cursor=db.rawQuery("select * from beacon",null);
                //    Toast.makeText(MainActivity.this, cursor.getCount()+minors, Toast.LENGTH_SHORT).show();
                    if (cursor != null) {

                        // move cursor to first row

                        if (cursor.moveToFirst()) {

                            do {

                                // Get version from Cursor

                              //  Toast.makeText(MainActivity.this,"0"+cursor.getString(cursor.getColumnIndex("outlet"))+"0", Toast.LENGTH_SHORT).show();
outlet.add(cursor.getString(cursor.getColumnIndex("outlet")));
                                distance.add(String.format("%.2g%n", Utils.computeAccuracy(beacon)));


                                // add the bookName into the bookTitles ArrayList

                                //bookTitles.add(bookName);

                                // move to next row

                            } while (cursor.moveToNext());

                        }

                    }
                    cursor.close();









               /*    // Toast.makeText(MainActivity.this, "uu"+uuids, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(MainActivity.this, "ma"+majors, Toast.LENGTH_SHORT).show();
                   // Toast.makeText(MainActivity.this, "mi"+minor, Toast.LENGTH_SHORT).show();
              //      Toast.makeText(MainActivity.this, beacon.getMajor()+"", Toast.LENGTH_SHORT).show();
                    try {
                        beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                   // distance.add(String.format("%.2g%n", Utils.computeAccuracy(beacon)));
                    RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
                    StringRequest sr = new StringRequest(Request.Method.POST, "http://demo.roclomos.com/screen/find.php", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //mPostCommentResponse.requestCompleted();
                            // lvb.setEnabled(true);

                            rqc--;
                           if(!response.equalsIgnoreCase("empty"))
                            {
//                                Toast.makeText(MainActivity.this,response.substring(0,response.lastIndexOf(" ")),Toast.LENGTH_LONG).show();
                                 //String outt=response.split("@#$")[0];
                                //       Integer pos = Integer.parseInt(response.split("@#$")[1]);
                                outlet.add(response.substring(0,response.lastIndexOf(" ")));
                                distance.add(response.substring(response.lastIndexOf(" ")));
                                //return;
                            }

                           // String outt=response.split("@#$")[0];
                     //       Integer pos = Integer.parseInt(response.split("@#$")[1]);
                            //outlet.add(outt);
                            //distance.add(response.split("@#$")[1]);
                            //Toast.makeText(MainActivity.this, outt+"   "+response.split("@#$")[1], Toast.LENGTH_SHORT).show();
                            if(rqc==0) {
                                ListView lv = (ListView) findViewById(R.id.listview);
                                lv.setAdapter(new ListAdapter(MainActivity.this, outlet, distance));
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //mPostCommentResponse.requestEndedWithError(error);
                            Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();

                            //  Spinner spn =(Spinner)findViewById(R.id.spinner);
                            // String names= spn.getSelectedItem().toString();
                            // String ids = id.get(name.indexOf(names));
                            // params.put("name",names);
                            // params.put("id",ids);
                           // majors=+"";
                            minor=beacon.getMinor()+"";
                            distan= String.format("%.2g%n", Utils.computeAccuracy(beacon));


                            params.put("uuid", " "+beacon.getProximityUUID());
                            params.put("major", " "+beacon.getMajor());
                            params.put("minor", " "+beacon.getMinor() );
                            params.put("id", i+"");
                            params.put("distance",String.format("%.2g%n", Utils.computeAccuracy(beacon)));
                            // params.put("mac",bmac.getText().toString());
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Content-Type", "application/x-www-form-urlencoded");
                            return params;
                        }};


                    queue.add(sr);

                    // locateall.this.cdown.start();
             */

                                }
                ListView lv = (ListView) findViewById(R.id.listview);
                lv.setAdapter(new ListAdapter(MainActivity.this, outlet, distance));

                // tv.setVisibility(View.VISIBLE);
            }
        });






    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void quit(View v) {
        this.finish();
    }

    public void download(View v)
    {
        Toast.makeText(MainActivity.this, "Starting Download", Toast.LENGTH_SHORT).show();
String qry;
        qry = "drop table IF exists beacon ";
db.execSQL(qry);
        qry="create table beacon(outlet varchar(200),uuid varchar(50),major varchar(50),minor varchar(50))";
        db.execSQL(qry);
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        StringRequest sr = new StringRequest(Request.Method.POST, "http://demo.roclomos.com/screen/getall.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                File path = MainActivity.this.getExternalFilesDir(null);
                File file = new File(path, "scanbeacon.txt");
                FileOutputStream stream = null;
                try {
                    stream = new FileOutputStream(file);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    stream.write(response.getBytes());
                }catch (IOException e) {
                    e.printStackTrace();
                }  finally {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
           //     Toast.makeText(MainActivity.this, ""+response.split("12345").length, Toast.LENGTH_SHORT).show();
String[] rows=response.split("12345");
int i=1;
                for(String s : rows) {

                    if (s.isEmpty())
                        continue;
                    i++;
                    //    Toast.makeText(MainActivity.this, s+"", Toast.LENGTH_SHORT).show();
                    //  Toast.makeText(MainActivity.this, ""+s.split("@@@").length, Toast.LENGTH_SHORT).show();
                    String uuid = s.split("@@@")[1].trim(), major = s.split("@@@")[2].trim(), minors = s.split("@@@")[2].trim(), outlet = s.split("@@@")[0];
                    String m = s.split("@@@")[3].replace(" ", "");
                    m = " " + m;
                    String qry = "insert into beacon values('" + s.split("@@@")[0] + "','" + s.split("@@@")[1] + "','" + s.split("@@@")[2] + "','" + m + "')";

                    db.execSQL(qry);
                }
                    Toast.makeText(MainActivity.this, "Download Complete", Toast.LENGTH_SHORT).show();
                 //   Cursor cursor =db.rawQuery("select * from beacon",null);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mPostCommentResponse.requestEndedWithError(error);
                Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


        queue.add(sr);




    }



public void findb(View v)
{
    Toast.makeText(MainActivity.this, "Detecting Beacons", Toast.LENGTH_SHORT).show();
    beaconManager.connect(new
                                  BeaconManager.ServiceReadyCallback() {
                                      @Override
                                      public void onServiceReady() {
                                          try {
                                              beaconManager.startRanging(
                                                      ALL_ESTIMOTE_BEACONS);
                                              //locateall.this.cdown.start();
                                          } catch (RemoteException e) {
                                              //Log.e(TAG,
                                              //      "Cannot start ranging", e);
                                          }
                                      }

                                  }
    );
}



    @Override
    protected void onResume()
    {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }

        super.onResume();
    }


    public class ListAdapter extends BaseAdapter {
        List<String> result,distance;
        Context context;

        private LayoutInflater inflater=null;
        public ListAdapter(MainActivity mainActivity, List<String> outlet,List<String> dist) {
            // TODO Auto-generated constructor stub
            result=outlet;
            context=mainActivity;
            distance=dist;
            //  major=major;
            inflater = ( LayoutInflater )context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public class Holder
        {
            TextView outlet;

            TextView distance;
        }
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            Holder holder=new Holder();
            View rowView;
            rowView = inflater.inflate(R.layout.list_row, null);
            holder.outlet=(TextView) rowView.findViewById(R.id.outlet);
            holder.distance=(TextView) rowView.findViewById(R.id.distan);
            holder.distance.setText("Distance: "+distance.get(position)+"m");
            holder.outlet.setText("Outlet: "+result.get(position));
            //holder.major.setImageResource(imageId[position]);
            return rowView;

        }

    }




}

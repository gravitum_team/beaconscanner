package com.roclomos.screen.beaconscanner;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class addoutlet extends AppCompatActivity {
int id;
    SQLiteDatabase db;

    @Override
    public void onBackPressed()
    {
        Intent mainIntent = new Intent(addoutlet.this, Main2Activity.class);
        this.startActivity(mainIntent);
        this.finish();
        // code here to show dialog
        super.onBackPressed();  // optional depending on your needs
    }

    public void save(View v) {

        if(edn.getText().toString().isEmpty() ||edn.getText().toString().isEmpty() ||edn.getText().toString().isEmpty()  )
        {
            Toast.makeText(addoutlet.this, "Enter all values", Toast.LENGTH_SHORT).show();
            return;
        }
        Cursor cr = db.rawQuery("select * from beacon",null);
id = cr.getCount();
        db.execSQL("insert into beacon values('"+edn.getText().toString()+"','"+id+"')");





        RequestQueue queue = Volley.newRequestQueue(addoutlet.this);
        StringRequest sr = new StringRequest(Request.Method.POST,"http://demo.roclomos.com/screen/insertoutlet.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
              //  lvb.setEnabled(true);
                Toast.makeText(addoutlet.this, response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mPostCommentResponse.requestEndedWithError(error);
                Toast.makeText(addoutlet.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

            //    Spinner spn =(Spinner)findViewById(R.id.spinner);
                String names= edn.getText().toString();

                String area = eda.getText().toString();
                String category = edc.getText().toString();
                String y;
                if(rb.isSelected())
                    y="yes";
                else
                y="no";
                params.put("name",names);
                params.put("area",area);
                params.put("category",category);
                params.put("has",y);
                params.put("id",id+"");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);




    }
    EditText edn,eda,edc,ed;
    RadioButton rb,rb2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addoutlet);
         rb= (RadioButton)findViewById(R.id.radioButton);
        rb.setSelected(true);
        db=openOrCreateDatabase("beaconoutlet", Context.MODE_PRIVATE, null);

       edn = (EditText) findViewById(R.id.oname);
      eda = (EditText) findViewById(R.id.area);
      edc = (EditText) findViewById(R.id.category);

       // RadioButton rb= (RadioButton)findViewById(R.id.radioButton);
         rb2= (RadioButton)findViewById(R.id.radioButton2);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_addoutlet, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

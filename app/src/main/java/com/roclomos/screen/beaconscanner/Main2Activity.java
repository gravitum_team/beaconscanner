package com.roclomos.screen.beaconscanner;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class Main2Activity extends AppCompatActivity {

    SQLiteDatabase db;
    public void opendetect(View v) {
        Intent mainIntent = new Intent(Main2Activity.this, MainActivity.class);
        this.startActivity(mainIntent);
        this.finish();
    }


    public void add(View v) {
        Intent mainIntent = new Intent(Main2Activity.this, addoutlet.class);
        this.startActivity(mainIntent);
        this.finish();
    }



    public void syncDB(View v) throws IOException {
        String qry;
        Toast.makeText(Main2Activity.this, "Starting Sync..", Toast.LENGTH_SHORT).show();
        qry = "drop table IF exists beacon ";
        db.execSQL(qry);
        qry="create table beacon(outlet varchar(200),oid varchar(50))";
        db.execSQL(qry);
        RequestQueue queue = Volley.newRequestQueue(Main2Activity.this);
        //String pname =((EditText)findViewById(R.id.edName)).getText().toString().replace(" ","lmpno");
        String url ="http://demo.roclomos.com/screen/center.php";//name="+pname+"&id="+id;
// Request a string response from the provided URL.!@#$

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //mTextView.setText("Response is: "+ response.substring(0,500));
                        byte ptext[] = response.getBytes();
                        String value="";
                        try {
                            value = new String(ptext, "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        String[] lists = value.split("12345");
                        //      Toast.makeText(MainActivity.this, lists+ "", Toast.LENGTH_LONG).show();
                        for(String s:lists)
                        {
                            // Toast.makeText(MainActivity.this, s+ "", Toast.LENGTH_LONG).show();
                            if(!s.isEmpty())
                            {
                                String qry = "insert into beacon values('" + s.split("@@@")[0] + "','" + s.split("@@@")[1]+"')";

                                db.execSQL(qry);
                                //   name.add(s.split("@@@")[0]);
                                //   id.add(s.split("@@@")[1]);
                            }
                        }
                        Toast.makeText(Main2Activity.this,  "Sync Complete", Toast.LENGTH_SHORT).show();
                        //ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_spinner_item, MainActivity.this.name);
                        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Spinner spn = (Spinner) findViewById(R.id.spinner);
                        //spn.setAdapter(dataAdapter);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Main2Activity.this, "Please Check your Internet Connection!", Toast.LENGTH_SHORT).show();
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);







    }





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        db=openOrCreateDatabase("beaconoutlet", Context.MODE_PRIVATE, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

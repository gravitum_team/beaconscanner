package com.roclomos.screen.beaconscanner;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    SQLiteDatabase db;
    private static Region ALL_ESTIMOTE_BEACONS = new Region("regionId", null, null, null);
    private BeaconManager beaconManager;// = new BeaconManager();
    List<String> name,id;
ListView lvb;
     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         db=openOrCreateDatabase("beaconoutlet", Context.MODE_PRIVATE, null);
        name=new ArrayList<String>();
        id=new ArrayList<String>();
lvb=(ListView) findViewById(R.id.listView);

lvb.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        lvb.setEnabled(false);
      //  Toast.makeText(MainActivity.this,  adapterView.getSelectedItem(), Toast.LENGTH_SHORT).show();
View row = view;
    final    TextView ma = (TextView)row.findViewById(R.id.MajorID);
        final    TextView mi = (TextView)row.findViewById(R.id.MinorID);
        final TextView uu = (TextView)row.findViewById(R.id.UUID);
        final TextView bmac = (TextView)row.findViewById(R.id.mac);
        final TextView bname = (TextView)row.findViewById(R.id.bname);
        final TextView bdist = (TextView)row.findViewById(R.id.bdist);
        final TextView brssi = (TextView)row.findViewById(R.id.brssi);
        final TextView about = (TextView)findViewById(R.id.abouttxt);
        Toast.makeText(MainActivity.this,"Processing...", Toast.LENGTH_SHORT).show();

        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        StringRequest sr = new StringRequest(Request.Method.POST,"http://demo.roclomos.com/screen/insert.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                lvb.setEnabled(true);
                Toast.makeText(MainActivity.this, response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mPostCommentResponse.requestEndedWithError(error);
                Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();

                Spinner spn =(Spinner)findViewById(R.id.spinner);
                String names= spn.getSelectedItem().toString();
                String ids = id.get(name.indexOf(names));
                params.put("name",names);
                params.put("id",ids);
                params.put("uuid",uu.getText().toString().replace("UUID:",""));
                params.put("major",ma.getText().toString().replace("Major:", ""));
                params.put("minor",mi.getText().toString().replace("Minor:", ""));
                params.put("bname",bname.getText().toString().replace("Name:", ""));
                params.put("mac",bmac.getText().toString().replace("Mac Add:", ""));
                params.put("bdist",bdist.getText().toString().replace("Distance:", ""));
                params.put("rssi",brssi.getText().toString().replace("RSSI:", ""));
                params.put("about",about.getText().toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);

    }
});
         String[] columns ={"outlet","oid"};
      //   Cursor cursors = db.query("beacon", columns, null, null, null, null, null);

         Cursor cursors = db.rawQuery("select * from beacon",null);
//         Toast.makeText(MainActivity.this,  cursors.getColumnCount()+"", Toast.LENGTH_SHORT).show();
         if (cursors != null) {

             // move cursor to first row

             if (cursors.moveToFirst()) {

                 do {

                     // Get version from Cursor

                     //  Toast.makeText(MainActivity.this,"0"+cursor.getString(cursor.getColumnIndex("outlet"))+"0", Toast.LENGTH_SHORT).show();
                     name.add(cursors.getString(0));
                     id.add(cursors.getString(1));


                     // add the bookName into the bookTitles ArrayList

                     //bookTitles.add(bookName);

                     // move to next row

                 } while (cursors.moveToNext());

             }

         }
         cursors.close();
         ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_spinner_item, MainActivity.this.name);
         dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
         Spinner spn = (Spinner) findViewById(R.id.spinner);
         spn.setAdapter(dataAdapter);


  /*      RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        //String pname =((EditText)findViewById(R.id.edName)).getText().toString().replace(" ","lmpno");
        String url ="http://demo.roclomos.com/screen/center.php";//name="+pname+"&id="+id;
// Request a string response from the provided URL.!@#$

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //mTextView.setText("Response is: "+ response.substring(0,500));
                        byte ptext[] = response.getBytes();
                        String value="";
                        try {
                            value = new String(ptext, "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        String[] lists = value.split("12345");
                  //      Toast.makeText(MainActivity.this, lists+ "", Toast.LENGTH_LONG).show();
                        for(String s:lists)
                        {
                           // Toast.makeText(MainActivity.this, s+ "", Toast.LENGTH_LONG).show();
                            if(!s.isEmpty())
                            {
                                name.add(s.split("@@@")[0]);
                                id.add(s.split("@@@")[1]);
                            }
                        }
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_spinner_item, MainActivity.this.name);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        Spinner spn = (Spinner) findViewById(R.id.spinner);
                        spn.setAdapter(dataAdapter);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Please Check your Internet Connection!", Toast.LENGTH_SHORT).show();
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

*/






        beaconManager= new BeaconManager(getApplicationContext());
        beaconManager.setBackgroundScanPeriod(
                TimeUnit.SECONDS.toMillis(1), 0);



        beaconManager.setRangingListener(new BeaconManager.RangingListener() {

            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> beacons) {
          //      TextView tv = (TextView) findViewById(R.id.loctxt);
            //    tv.setVisibility(View.INVISIBLE);
                String be = "", diste = "", major = "";
                // List<Integer> dist = new ArrayList<Integer>();
                int count = 0;
                double dist = 0;
                Beacon closest = null;
                List<String> UUID = new ArrayList<String>();
                List<String> majorid = new ArrayList<String>();
                List<String> minor = new ArrayList<String>();
                List<String> namel = new ArrayList<String>();
                List<String> macl = new ArrayList<String>();
                List<String> rssi = new ArrayList<String>();
                List<String> distance = new ArrayList<String>();
                List<String> proxi = new ArrayList<String>();
                // Toast.makeText(locateall.this, "none", Toast.LENGTH_SHORT).show();
                int i = 0;
                for (Beacon beacon : beacons) {
                    ListView lv = (ListView) findViewById(R.id.listView);
                    lv.setAdapter(null);
                  //  locateall.this.cdown.cancel();
                    Utils.Proximity prx = Utils.computeProximity(beacon);
                    majorid.add("" + beacon.getMajor());
                    UUID.add("" + beacon.getProximityUUID());
                    minor.add("" + beacon.getMinor());
                    macl.add(("" + beacon.getMacAddress()));
rssi.add("" + beacon.getRssi());
                    proxi.add(Utils.computeProximity(beacon) + "");

                    distance.add(String.format("%.2g%n", Utils.computeAccuracy(beacon)));
                    namel.add(("" + beacon.getName()));
                   // locateall.this.cdown.start();
                }

                ListView lv = (ListView) findViewById(R.id.listView);
                lv.setAdapter(new ListAdapter(MainActivity.this, UUID, majorid, minor,macl, namel,distance,rssi,proxi));

               // tv.setVisibility(View.VISIBLE);
            }
        });








    }

    @Override
    public void onBackPressed()
    {
        Intent mainIntent = new Intent(MainActivity.this, Main2Activity.class);
        this.startActivity(mainIntent);
        this.finish();
        // code here to show dialog
        super.onBackPressed();  // optional depending on your needs
    }




    @Override
    protected void onResume()
    {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
        beaconManager.connect(new
                                      BeaconManager.ServiceReadyCallback() {
                                          @Override
                                          public void onServiceReady() {
                                              try {
                                                  beaconManager.startRanging(
                                                          ALL_ESTIMOTE_BEACONS);
                                                  //locateall.this.cdown.start();
                                              } catch (RemoteException e) {
                                                  //Log.e(TAG,
                                                    //      "Cannot start ranging", e);
                                              }
                                          }

                                      }
        );
        super.onResume();
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





    public class ListAdapter extends BaseAdapter {
        List<String> result,major,minor,macls,namebs,bdists,rssis,proxis;
        Context context;

        private  LayoutInflater inflater=null;
        public ListAdapter(MainActivity mainActivity, List<String> UUID, List<String> majorc, List<String> minorc,List<String> macl,List<String> namebl,List<String> bdist,List<String> rssi,List<String> proxia) {
            // TODO Auto-generated constructor stub
            result=UUID;
            this.major=majorc;
            minor=minorc;
            macls=macl;
            namebs=namebl;
            context=mainActivity;
            bdists=bdist;
            rssis=rssi;
            proxis=proxia;
            //  major=major;
            inflater = ( LayoutInflater )context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public class Holder
        {
            TextView UUID;
            TextView major;
            TextView minor;
            TextView mact;
            TextView namebt;
            TextView dist;
            TextView rssi;
            TextView prosabc;
        }
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            Holder holder=new Holder();
            View rowView;
            rowView = inflater.inflate(R.layout.list_row, null);
            holder.UUID=(TextView) rowView.findViewById(R.id.UUID);
            holder.major=(TextView) rowView.findViewById(R.id.MajorID);
            holder.minor=(TextView) rowView.findViewById(R.id.MinorID);
            holder.mact=(TextView) rowView.findViewById(R.id.mac);
            holder.dist=(TextView) rowView.findViewById(R.id.bdist);
            holder.rssi=(TextView) rowView.findViewById(R.id.brssi);
            holder.namebt=(TextView) rowView.findViewById(R.id.bname);
            holder.prosabc=(TextView) rowView.findViewById(R.id.prox);
            holder.dist.setText("Distance: "+bdists.get(position)+"m");
            holder.rssi.setText("RSSI: "+rssis.get(position));
            holder.UUID.setText("UUID: "+result.get(position));
            holder.major.setText("Major: "+ major.get(position));
            holder.minor.setText("Minor: "+minor.get(position));
            holder.namebt.setText("Name: " + namebs.get(position));
            holder.mact.setText("Mac Add: "+macls.get(position));
            holder.prosabc.setText("Proximity: " + proxis.get(position));
            //holder.major.setImageResource(imageId[position]);

            return rowView;
        }

    }

}
